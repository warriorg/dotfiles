HOME=${HOME}
VUNDLE=${HOME}"/.vim/bundle/Vundle.vim"


# Vim install `Vundle` and plugins
install_vundle(){
	if [ -d "${VUNDLE}" ]; then
		cd "${VUNDLE}"
		echo "Change directory to `pwd`"
		echo "${VUNDLE} exists. Git pull to update..."
		git pull
		cd - > /dev/null 2>&1
		echo "Change directory back to `pwd`"
	else
		echo "${VUNDLE} not exists. Git clone to create..."
		git clone https://github.com/gmarik/Vundle.vim.git ${VUNDLE}
		vim +PluginInstall +qall
	fi
}

main(){
	install_vundle

    cp vimrc ${HOME}/.vimrc
    cp vimrc.bundles ${HOME}/.vimrc.bundles
}

main

echo "[SETUP OK]"
