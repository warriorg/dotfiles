local key2App = {
    c = "Google Chrome",
    t = "iTerm",
    q = "QQ",
    m = "MacVim",
    e = "Evernote",
    a = "Atom",
    s = "DataGrip",
    j = "IntelliJ IDEA"
}

for key, app in pairs(key2App) do 
    hs.hotkey.bind({'cmd', 'alt', 'shift'}, key, function()
        hs.application.launchOrFocus(app)
    end)
end
